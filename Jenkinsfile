pipeline {
	
  agent any
	 
  tools { 
	   maven 'Maven-3.5.3'
  }
		
   //agent {docker { image 'valuedriven/jenkins-agent:1' }}
	
    environment {

        Settings = "--settings ${env.PROJECT_FILE_SETTINGS}"

        SonarHost = "${env.QUALITY_REPO_PROTOCOL}${env.QUALITY_REPO_HOST}:${env.QUALITY_REPO_PORT}"

        RepoHost = "${env.PROJECT_REPO_HOST}/${env.PROJECT_REPO_USER}/${env.PROJECT_REPO_NAME}.git"
		
		EmailRecipients = "${env.MAIL_RECIPIENTS}"         
    }

    options {

       buildDiscarder(logRotator(numToKeepStr: '10')) 
       disableConcurrentBuilds() 
       disableResume()
       timeout(time: 1, unit: 'DAYS')
	   durabilityHint('PERFORMANCE_OPTIMIZED')
	   //skipDefaultCheckout()
       
    }

    //triggers { pollSCM('H/15 9/18 * * * 1-5') } //disponível apenas na versão 2.22 do Jenkins. Aguardar para atualizar o container

    stages {

      stage("Commit") {

           steps {

             withCredentials([usernamePassword(credentialsId: 'repo', passwordVariable: 'PROJECT_REPO_PASSWORD', usernameVariable: 'PROJECT_REPO_USER')]) {

                sh "git config --global credential.username ${PROJECT_REPO_USER}"
      
                sh "git config --global credential.helper '!echo password=${PROJECT_REPO_PASSWORD}; echo'"
								
             }             
             
			 selecionarBranch()
			 
			 script {
			    if (env.QUALITY_REPO_ENABLED == 'True') 
				  { sh "mvn sonar:sonar -Dsonar.host.url=${SonarHost} ${Settings}"}
				else
				  { sh "echo ===> Servidor de qualidade desabilitado"}
			 }
			 
			 script {
			    if (env.BRANCH_SELECIONADA == 'master') {				 
				   sh "git checkout -b $PROJECT_NAME-R-$VERSION_NUMBER-$BUILD_NUMBER"
				   tagName = "v$VERSION_NUMBER-$BUILD_NUMBER"
			    }
			    else {
				   sh 'git checkout "$BRANCH_SELECIONADA"'
				   Branch = "${env.BRANCH_SELECIONADA}"
				   Projeto = "${env.PROJECT_NAME}"
				   NumeroBuild = "${env.BUILD_NUMBER}"
				   tagName = "v"+Branch.substring(Projeto.length()+2,Branch.length())+"-fix-"+NumeroBuild
			    }
			 }
			 
             sh "mvn versions:set -DnewVersion=$VERSION_NUMBER-$BUILD_NUMBER ${Settings}"
          
             sh "mvn clean ${Settings}"
          
             sh "mvn install ${Settings}"
                    
             script {
                if (env.ARTIFACT_REPO_ENABLED == 'True') 
					{sh "mvn deploy ${Settings}"}
				else
				  { sh "echo ===> Repositório de artefatos desabilitado"}

		     }
		 			 
             sh "git commit -a -m 'Nova release candidata'"
			 			 
			 echo "====> nome da etiqueta ${tagName}"
			 sh "git tag -a ${tagName} -m 'Nova versão'"
	
    		 sh	"git push https://${PROJECT_REPO_USER}@${RepoHost} --follow-tags"
			 			 
	         script {				   
		        if (env.APP_SERVER_DEV_ENABLED == 'True') {			  
                  sh "mvn wildfly:deploy ${Settings}"}
				else
					{ sh "echo ===> Servidor de aplicação de desenvolvimento desabilitado"}  
		     }

				 				 				                    
           }
                   
      }
            
      stage("Aceitação") {
		  
           steps {
			   
		      script {				   
		       if (env.APP_SERVER_TEST_ENABLED == 'True') {
                  sh "mvn wildfly:deploy ${Settings}"
                  sh "mvn failsafe:integration-test ${Settings}"}
			   else
				  { sh "echo ===> Servidor de aplicação de testes desabilitado"}
		      }
			 
			  input ( message: 'Autoriza promoção da build para homologação?', ok: 'Autorizado', submitter: 'admin' )
           }

      }

     stage("Homolocação") {
		  
           steps {			   
		      script {				   
		        if (env.APP_SERVER_HOM_ENABLED == 'True') {
                  sh "mvn wildfly:deploy ${Settings}"}
				else
				  { sh "echo ===> Servidor de aplicação de homologação desabilitado"}
		     }
			 
			 input ( message: 'Autoriza promoção da build para produção?', ok: 'Autorizado', submitter: 'admin' )
           }

      }
	 
	  stage("Produção") {
	   	         
         steps {

		    script {					  
			   if (env.APP_SERVER_PROD_ENABLED == 'True') {
				  sh "mvn wildfly:deploy ${Settings}"}
			   else
				  { sh "echo ===> Servidor de aplicação de produção desabilitado"}
			}

         }
	  }

    }

    post {

        always {
        
            deleteDir()
           
            enviarNotificacao()
           
        }
		
        success {			
            echo "Pipeline concluído com sucesso"			
        }
		
        unstable {			
            echo "Pipeline apresenta instabilidade"
        }
		
        failure {        
             echo "Erro na execução do pipeline"                        
        }
        changed {
            echo "O pipeline teve a execução atual diferente da última"
        }
    }

}

void carregarVariavelAmbiente(String parametro, String arquivo) {
	
	valorParametro = sh (
		script: "cat ${arquivo} | grep ${parametro} | cut -d'=' -f2-",
		returnStdout: true
	)
	
	sh (
		script: "export $parametro=$valorParametro",
		returnStdout: false
	)	
		 
}

void enviarNotificacao() {
	
	def jobName = currentBuild.fullDisplayName

	emailext body: '''${SCRIPT, template="email.template"}''',
    subject: "[Jenkins] ${jobName}",
    to: "${EmailRecipients}",
    replyTo: "${EmailRecipients}",
    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
}

void selecionarBranch() {
	
	sh 'git branch -r | awk \'{print $1}\' ORS=\'\\n\' >branches.txt'
	
	sh '''cut -d '/' -f 2 branches.txt > branch.txt'''
	 
	branches = readFile "branch.txt"
	
	echo "Selecione a branch para submeter ao pipeline"
	
	env.BRANCH_SELECIONADA = input message: 'Escolha a branch', ok: 'Validar',
	parameters: [choice(name: 'BRANCH_NAME', choices: "${branches}", description: 'Branch para pipeline?')]
	
}
